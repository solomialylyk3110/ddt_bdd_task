package com.solomi.utils.entity;

import com.solomi.utils.json.JsonReader;

public class UserFromJson {
    private JsonReader jsonReader = new JsonReader();

    public UserFromJson() {
    }

    public String getName() {
        return jsonReader.getElementFromJson("name");
    }

    public String getPassword() {
        return jsonReader.getElementFromJson("password");
    }

    public String getLogin() {
        return jsonReader.getElementFromJson("login");
    }
}
