package com.solomi.utils.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
    private final static String PATH_PROPERTIES = "src/main/resources/path.properties";
    private static Properties pro;

    private PropertiesReader(){}

    public static String getProperty(String nameProperty) {
        pro = new Properties();
        try {
            pro.load(new FileInputStream(PATH_PROPERTIES));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pro.getProperty(nameProperty);
    }
}
