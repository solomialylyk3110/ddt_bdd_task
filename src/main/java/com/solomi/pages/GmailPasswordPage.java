package com.solomi.pages;

import com.solomi.decorator.Button;
import com.solomi.decorator.Input;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailPasswordPage extends AbstractPage {
    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInput;

    @FindBy(id = "passwordNext")
    private WebElement passwordNext;

    private static Logger logger = LogManager.getLogger(GmailPasswordPage.class);

    public void typePasswordAndSubmit(String password) {
        logger.info("Try to type password and submit");
        new Input(passwordInput).waitUntilVisibleAndSendKeys(password);
        try {
            new Button(passwordNext).waitUntilVisibleAndClick();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }
}
