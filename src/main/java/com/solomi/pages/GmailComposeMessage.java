package com.solomi.pages;

import com.solomi.decorator.Input;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailComposeMessage extends AbstractPage{
    @FindBy(css = "div.T-I.T-I-KE.L3")
    private WebElement composeButton;

    @FindBy(className = "vO")
    private WebElement sendToField;

    @FindBy(xpath = "//*[@role = 'textbox']")
    private WebElement emailBodyField;

    @FindBy(xpath = "//*[@name = 'subjectbox']")
    private WebElement subjectField;

    @FindBy(className = "Ha")
    private WebElement saveAndCloseButton;

    private static Logger logger = LogManager.getLogger(GmailComposeMessage.class);

    public void composeAndSaveMessage(String sendTo, String subject, String body) {
        logger.info("Click on compose button");
        composeButton.click();



        logger.info("Fill in the sendTo field");
        new Input(sendToField).clearClickAndSEndKeys(sendTo);   // decorator instead :
                                                                //        sendToField.click();
                                                                //        sendToField.clear();
                                                                //        sendToField.sendKeys(sendTo);

        logger.info("Fill in the bodyMessage field");
        new Input(emailBodyField).clearClickAndSEndKeys(body);  // decorator instead:
                                                                //        emailBodyField.click();
                                                                //        emailBodyField.clear();
                                                                //        emailBodyField.sendKeys(body);

        logger.info("Fill in the subject field");
        new Input(subjectField).clearClickAndSEndKeys(subject); // decorator instead:
                                                                //        subjectField.click();
                                                                //        subjectField.clear();
                                                                //        subjectField.sendKeys(subject);

        logger.info("Click on SaveAndCloseButton");
        saveAndCloseButton.click();

    }

}
