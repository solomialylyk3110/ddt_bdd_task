package com.solomi.pages;

import com.solomi.factory.DriverManager;
import org.openqa.selenium.support.PageFactory;

abstract public class AbstractPage {
    public AbstractPage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }
}
