package com.solomia.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features ={"src/test/resources/cucumberGmail.feature"}, glue = {"com.solomia.step"})
public class RunTest extends AbstractTestNGCucumberTests {
}
