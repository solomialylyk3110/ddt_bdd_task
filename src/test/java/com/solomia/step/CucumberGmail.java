package com.solomia.step;

import com.solomi.business.BusinessObject;
import com.solomi.utils.entity.User;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import static com.solomi.constants.Constants.*;

public class CucumberGmail {
    private static Logger logger = LogManager.getLogger(CucumberGmail.class);
    BusinessObject businessObject = new BusinessObject();

    @When("^user with name (.*) log in using login as (.*[@gmail\\.com]) and password as (.*)")
    public void login(String name, String login, String password){
        User user = new User(name, login, password);
        logger.info("Test with login and password");
        businessObject.login(user);
        Assert.assertEquals(businessObject.getCurrentGoogleAccountName(), user.getName(), "No login was performed in the system");
    }

    @Then("^user compose a message$")
    public void composeDraftMessage(){
        logger.info("Test about compose letter and save it");
        businessObject.createDraftMessage(MESSAGE_SEND_TO, MESSAGE_SUBJECT, MESSAGE_BODY);
    }

    @And("^user go to draft folder and check the message$")
    public void checkDraftMessage() {
        logger.info("Subject test with the Draft Page");
        String textMessage = businessObject.getFirstMessageTextFromDraft();
        Assert.assertTrue(textMessage.contains(MESSAGE_SUBJECT), "There is no matching with subject title and the letter");
    }

    @Then("^user send emil$")
    public void sendMessage() {
        Assert.assertTrue(businessObject.clickOnSendButtonAndCheckIfSuccessfully(), "Can't send the letter");
    }
}
