package com.solomia.step;

import com.solomi.business.BusinessObject;
import com.solomi.factory.DriverManager;
import com.solomi.utils.entity.User;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;

public class Hooks {
    WebDriver driver;

    @Before
    public void beforeScenario(){
        System.out.println("This will run before the Scenario");
        driver = DriverManager.getDriver();
    }

    @After
    public void afterScenario(){
        System.out.println("This will run after the Scenario");
        driver.quit();
    }
}
