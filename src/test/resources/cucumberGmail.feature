Feature: Login gmail, save a message to Draft folder and send it

  Scenario Outline: Send draft email
    When user with name <name> log in using login as <login> and password as <password>
    Then user compose a message
    And user go to draft folder and check the message
    Then user send emil


    Examples:
    | name          | login                     |password    |
    |Selenium User  | seleniumuser17@gmail.com  |selenium17_ |